
local function configure(context)
    context.recurse()
end

local function setup(context)
    context.env["FOO"] = "bar"
    context.recurse()
end

print("Done!")

return {
    setup=setup,
    configure=configure
}
