# The konstrui project

Our goal is to make it easy to mix and match different languages and different
build systems. We belive that you should be able to have freedom to choose
your favorite tooling for every language separately.

## How this works

We aim to be a language neutral meta build system. Just by using konstrui you
won't be able to build anything. We just run different goals of other build
sytems in order to let you customize everything.

*We do it all in parallel by default.*

## Opinionated?

Yes. While do what we do we will try to point you at correct application
development lifecycle:

```
Build -> Assemble -> Package -> Persist -> Deploy
```

## How this is different from any other meta build system?

Every other build system (with an exception of pure `make`) evoved from being a
single language build system to being able to hadle most other languages. This
forces you to use particular stacks or split projects entirely.

What we want to achieve is to allow you to choose a comfortable stack that is
based on the common parts of your componets.

## Give me an example 

Let's consider a very typical stack. Java backend and javascipt frontend. What
we would normally use is either:
 - build 2 project separately using differnt build systems
 - build 2 project using java tooling 
 - build 2 project using javascript tooling

With konstrui you can discard these approaches and just build entire system
using one command (or few depending how you configure konstrui).

 1. Split the code into 2 directories
 2. Install a tooling of your choice for both of those directories. You can
    also automate this but we'll leave it to you.
 3. Prepare a `konstrui.lua` file in both of those directories. Define there a
    goals that you see fit for the project:
     - For npm javascipt project we will define: start, build, package and
       clean
     - For Java project with Maven build system we will define: build,
       assemble, package, and install.
    All of these are goals that you can often see in a project of particular
    language.
 4. Define a `konstrui.lua` in level above. Now let's see... Define a goals:
    package and build. We can also define a goal called "deploy" here. 

And like so we easily created a project that's neetly can be deployed to
artifact registires with one command:
```
konstrui build package assemble deploy
```
This command maybe a bit long but it is a single command that you can modify to 
exclude a particular stage. For example:
```
konstrui assemble deploy
```
Will only assemble javascipt application and deploy both js and java artifacts
to registires. This can shorten your build times by a lot if you don't need to
recompile. This is a common usecase for project admins. If you are a developer
maybe you only need to `build package`.

## Why not use make then?

 1. It doesn't have modules.
 2. It can't execute them in parallel.
 3. It doesn't provide native artifact operations.
 4. You write goals in shell which allows for too much and too little at times.

