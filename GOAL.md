
## Example layout

Okay so let's consider a particular project:

```
 A/
 |_ Ainside
 |_ A/B/
 |      |_ Binside
 |      \_ A/B/D/
 \_ A/C/
        |
        \_Cinside
```

## Goals defined

Folders A, B, C and D contain "konstrui.lua" file. Let's say they have these
goals defined:

### A
 - ASolo
 - ASharedAll
 - ASharedB
 - BwithSub
 - ASharedC

### B
 - ASharedAll
 - ASharedB
 - BSolo
 - BwithSub
 - BSharedC

### C
 - ASharedAll
 - ASharedB
 - BSharedC
 - CSolo

### D
 - ASharedAll
 - BwithSub

We refer to all directories containing `konstrui.lua` file as "projects".

## Rules

 1. You can only run goals from current WORK_PROJECT.
 2. Work project is found either in current work directory or it is first
    project up to 5 directories above current `$pwd`. Filesystem will also stop
    the search.
 3. Goals from current WORK_PROJECT cause execution of all subprojects by
    default.
 4. A goal is always executed first, then all subprojects execute that goal if
    they define them.
 5. The order of execution is always calculated at the very start of execution
    before acutal execution of goals. The calculated order is called reactor.
 6. The execution order is from bottom-up.
 7. Goals are named with alphanumeric strings. That and the project they belong
    to is their only identification.

## Example executions for project A

This project is WORK_PROJECT if we are running konstrui with work dir set
either to A itself or Ainside.

We can execute all defined goals for project A. Any other goal will end in
error and exit. You don't actually need to write any execution code for stage.
You can just make an empty definition. 

For `konstrui ASolo` reactor will look like this:

| Stage number | Projects executed (in parallel) |
|--------------|---------------------------------|
| 1            | A                               |

For `konstrui ASharedB` reactor will look like this:

| Stage number | Projects executed (in parallel) |
|--------------|---------------------------------|
| 1            | B                               |
| 2            | A                               |

For `konstrui ASharedAll` reactor will look like this:

| Stage number | Projects executed (in parallel) |
|--------------|---------------------------------|
| 1            | D                               |
| 2            | B, C                            |
| 3            | A                               |

## Example executions for project B

For `konstrui ASharedAll` reactor will look like this:

| Stage number | Projects executed (in parallel) |
|--------------|---------------------------------|
| 1            | D                               |
| 2            | B                               |

## Multistage executions for project A

For `konstrui ASharedAll ASharedB` reactor will look like this:

| Stage number | Projects executed (in parallel) |
|--------------|---------------------------------|
| 1            | D:ASharedAll                    |
| 2            | B:ASharedAll, C:ASharedAll      |
| 3            | A:ASharedAll                    |
| 4            | B:ASharedB                      |
| 5            | A:ASharedB                      |
