use colored::*;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::u64;

pub enum Log {
    INFO,
}


pub fn log(stream: &str, _level: Log, message: &str) {
    println!("{}{}", get_colored_prefix(stream), message);
}

fn get_colored_prefix(stream: &str) -> ColoredString {
    let prefix = format!("{:15}| ", stream).bold();
    let stream_hash: u64 = {
        let mut hasher = DefaultHasher::new();
        String::from(stream).hash(&mut hasher);
        hasher.finish()
    } % 10;
    let mut colored = match stream_hash {
        ..=2 => prefix.red(),
        ..=4 => prefix.yellow(),
        ..=6 => prefix.blue(),
        ..=8 => prefix.magenta(),
        ..=10 => prefix.cyan(),
        _ => prefix.bold()
    };
    if stream == "konstrui" {
        colored = colored.green();
    }
    colored
}
