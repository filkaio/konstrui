use std::path::Path;

mod project;
mod parser;
mod module;
mod lua;
mod log;

use crate::project::Project;
use crate::log::{Log, log};

pub(crate) type KResult<T> = Result<T, String>;
const FNAME: &str = "konstrui.lua";

fn main() -> KResult<()> {
    log("konstrui", Log::INFO, "Starting konstrui");
    let wanted = parser::parse()?;
    let project = Project::new(&Path::new("."))?;

    if wanted.len() == 0 {
        return Err("No targets specified".to_string());
    }
    
    for target in wanted {
        project.execute(target)?;
    }
    Ok(())
}
