use std::path::Path;
use std::fs;
use mlua::prelude::*;

use crate::KResult;


pub fn read_targets(script_path: &Path) -> Result<Vec<String>, Box<dyn std::error::Error>> { 
    let lua = Lua::new();
    println!("Reading script path: {:?}", script_path);

    let mut pairs = load_functions_from_file(&lua, script_path)?
        .pairs::<LuaString, LuaFunction>();

    if pairs.any(|p| p.is_err()) {
        return Err("Cannot convert tables to pairs (string, funtion)".into())
    }

    let lua_str_to_string = |lua_str: LuaString| lua_str.to_str().unwrap().to_string();

    Ok(
        pairs
            .map( |pair| pair.unwrap() )
            .map( |(key, _)| key )
            .map( lua_str_to_string )
            .collect()
    )
}

pub fn execute_function(script_path: &Path, function: &str) -> KResult<()> {
    //TODO
    Ok(())
}

fn load_functions_from_file<'a>(lua_runtime: &'a Lua, file_path: &Path) -> Result<LuaTable<'a>, LuaError> {
    let script = fs::read_to_string(file_path).unwrap();
    lua_runtime.load(&script).eval::<LuaTable>()
}

