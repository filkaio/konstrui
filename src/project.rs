use std::fs;
use std::rc::Rc;
use std::path::{Path, PathBuf};

use crate::{FNAME, KResult};
use crate::module::{Module, ExecutionError};
use crate::log::{log, Log};


#[derive(Debug)]
pub(crate) struct Project {
    current_module: Rc<Module>,
    top_module: Rc<Module>,
}

impl Project {

    pub(crate) fn new(path: &Path) -> KResult<Project> {
        let top_dir = Self::find_top_dir(path)?;
        let top_module = Rc::new(Self::module_from_path(top_dir.clone()));

        let msg =format!("Project's top directory found in: {}", top_dir.display());
        log("konstrui", Log::INFO, &msg);

        Ok(
            Project {
                current_module: top_module.clone(),
                top_module,
            }
        )
    }

    fn current_module(&self) -> &Module {
        &self.current_module
    }

    fn top_module(&self) -> &Module {
        &self.top_module
    }

    pub(crate) fn execute(&self, target: String) -> KResult<()> {
        use ExecutionError::{ExecutionFailed, TargetNoFound};

        match self.current_module().execute_target(&target) {
            Ok(_) => return Ok(()),
            Err(ExecutionFailed(str)) => return Err(str), 
            _ => (),
        }

        match self.top_module().execute_target(&target) {
            Ok(_) => return Ok(()),
            Err(ExecutionFailed(str)) => return Err(str), 
            Err(TargetNoFound) => return Err("Cannot find given target".to_string()),
        }
    }

    fn find_top_dir(base: &Path) -> KResult<PathBuf> {
        let module_path = ["./", "../", "../../"] .into_iter()
            .map(|str| Path::new(str))
            .map(|folder| [base, folder].into_iter().collect())
            .filter(|p: &PathBuf| is_konstrui_module(&p.as_path()))
            .nth(0);
            
        match module_path {
            Some(path) => Ok(Self::find_top_parent(path).canonicalize().unwrap()),
            None => Err("Could not find top directory of this project".into()),
        }
    }

    fn module_from_path(path: PathBuf) -> Module {
        let submodules = find_subdirectories(&path)
            .into_iter()
            .filter(|path| is_konstrui_module(path))
            .map(|module| Rc::new(Self::module_from_path(module)))
            .collect();

        let path = Path::new(&path);
        Module::new(path, submodules).unwrap()
    }

    fn find_top_parent(mut path: PathBuf) -> PathBuf {
        while path.parent().map(is_konstrui_module).unwrap_or(false) {
            path.pop();
        }
        path
    }
}

fn is_konstrui_module<P: AsRef<Path>> (path: P) -> bool {
    match fs::read_dir(path) {
        Err(_) => false,
        Ok(entries) => entries
            .into_iter()
            .filter_map(|result| result.ok())
            .any(|file| file.file_name() == FNAME),
    }
}

fn find_subdirectories(path: &Path) -> Vec<PathBuf> {
    fs::read_dir(path).unwrap()
        .into_iter()
        .filter_map(|result| result.ok())
        .filter(|file| file.metadata().is_ok())
        .filter(|file| file.metadata().unwrap().is_dir())
        .map(|dir| dir.path())
        .collect()
}

