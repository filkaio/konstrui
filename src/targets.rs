
type TargetResults = Result<Vec<Target>, String>;

trait ToStringError<T> {
    fn into_string_error(self) -> Result<T, String>;
}

impl<T> ToStringError<T> for LuaResult<T> {
    fn into_string_error(self) -> Result<T, String> {
        match self {
            Self::Err(err) => Err(err.to_string()),
            Self::Ok(t) => Ok(t),
        }
    }
}

fn fill(project: &mut Project) -> TargetResults {
    type TargetResultsHandles = Vec<thread::JoinHandle<TargetResults>>;
    let handles: TargetResultsHandles = project.top_module().into_vec().into_iter().map(|m| {
            let path: String = m.absolute_path.clone();
            thread::spawn(move || {
                run_lua_script(path)
            })
    }).collect();


    let results: Vec<TargetResults> = handles.into_iter()
        .map(|h| h.join().unwrap())
        .collect();

    match results.iter().find(|res| res.is_err()) {
            Some(err) => return err.clone(),
            None => (),
    }

    Ok(
        results
            .into_iter()
            .map(|r| r.unwrap())
            .reduce(|mut acc, mut v| { acc.append(&mut v); acc })
            .unwrap()
    )
}

fn run_lua_script(module_path: String) -> TargetResults {
    let script_path = Path::new(&module_path).join(Path::new(FNAME));
    let script = std::fs::read_to_string(script_path.clone()).unwrap();
    let lua = Lua::new();
    let target_funtions = lua.load(&script).eval::<LuaTable>().into_string_error()?;

    let mut targets = Vec::new();
    while target_funtions.len().unwrap() > 0 {
        let function = target_funtions.pop::<LuaFunction>().into_string_error()?;
        targets.push( Target {
                script_path: script_path.to_str().unwrap().to_string()
            }
        );
    }
    Ok(targets)
}
