use crate::{KResult, FNAME};
use std::thread;
use crate::lua;
use std::rc::Rc;
use crate::lua::execute_function;
use std::path::{Path, PathBuf};

pub(crate) enum ExecutionError {
    TargetNoFound,
    ExecutionFailed(String),
}

#[derive(Debug, Clone)]
pub(crate) struct Module {
    pub targets: Vec<Target>,
    pub children: Vec<Rc<Module>>,
}

#[derive(Debug, Clone)]
pub(crate) struct Target {
    script_path: PathBuf,
    function: String,
} 

impl Target {
    fn execute(&self) -> Result<(), ExecutionError> {
        match execute_function(&self.script_path, &self.function) {
            Ok(_) => Ok(()),
            Err(s) => Err(ExecutionError::ExecutionFailed(s)),
        }
    }
}

impl Module {

    pub fn new(path: &Path, children: Vec<Rc<Module>>) -> KResult<Module> {
        Ok(Module {
            targets: Module::read_targets(path)?,
            children
        })
    }

    fn read_targets(path: &Path) -> KResult<Vec<Target>> {
        let path: PathBuf = [path, Path::new(FNAME)].into_iter().collect();
        let targets: Vec<String> = match lua::read_targets(&path) {
            Ok(v) => v,
            Err(boxed_err) => return Err(boxed_err.to_string())
        };
        println!("{:?}",targets);
        Ok(
            targets
                .into_iter()
                .map(|function| Target { function, script_path: path.clone() })
                .collect()
        )
    }

    pub fn find_target(&self, target_name: &str) -> Option<&Target> {
        self.targets.iter().find(|t| t.function == target_name)
    }

    pub fn execute_target(&self, target_name: &str) -> Result<(), ExecutionError> {
        let target_found = self.find_target(target_name);
        if target_found.is_none() {
            return Err(ExecutionError::TargetNoFound);
        }
        let to_execute = {
            let mut tmp = self.children
                .iter()
                .map(|child| child.find_target(target_name))
                .filter(|opt| opt.is_some())
                .collect::<Vec<_>>();
            tmp.push(target_found);
            tmp
                .into_iter()
                .map(|opt| opt.unwrap())
                .collect::<Vec<_>>()
        };
        execute_on_threads(to_execute)
    }
}

fn execute_on_threads(targets: Vec<&Target>) -> Result<(), ExecutionError> {
    let thread_results: Vec<_> = targets
        .into_iter()
        .cloned()
        .map(|t| thread::spawn(
            move || t.execute()
        ))
        .map(|join_handle| join_handle.join())
        .collect();

    if thread_results.iter().any(|result| result.is_err()) {
        return Err(ExecutionError::ExecutionFailed(
            "Lua execution thread has panicked with unknown error".to_string()
        ));
    }

    let execution_results: Vec<_> = thread_results
        .into_iter()
        .map(|t_res| t_res.unwrap())
        .collect();
    for exec_res in execution_results {
        exec_res?;
    }
    Ok(())
}
